package com.orbitz.testscripts.lib;
import org.testng.annotations.Test;

import com.orbitz.pages.FlightsPage;
import com.orbitz.utility.Base;

public class TestCase_101 extends Base {

	FlightsPage flights;
	
	@Test
	public void flightBookingExecution() throws Exception {
		
		flights = new FlightsPage(driver);
		flights.flightBooking();
		System.out.println("TestCase_101 Executed");
	}
	
}
