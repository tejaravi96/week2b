package com.orbitz.testscripts.lib;

import org.testng.annotations.Test;

import com.orbitz.pages.HotelsPage;
import com.orbitz.utility.Base;

public class TestCase_102 extends Base{

	HotelsPage hotel;
	@Test
	public void hotelPageExecution() throws Exception {
		hotel = new HotelsPage(driver);
		hotel.hotelBooking();
		System.out.println("Test case 2 Excecuted");
	}
}
