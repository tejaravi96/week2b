package com.orbitz.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HotelsPage {

	@FindBy(id = "primary-header-hotel")
	WebElement hotelButton;

	@FindBy(id = "tab-hotel-tab")
	WebElement hotelOnlyButton;

	@FindBy(id = "hotel-destination")
	WebElement hotelDestination;

	@FindBy(id = "aria-option-1")
	WebElement dropDownOptionDestination;

	@FindBy(xpath =".//*[@id='hotel-checkin']")
	WebElement hotelCheckInDate;

	@FindBy(id = "hotel-checkout")
	WebElement hotelCheckOutDate;

	@FindBy(xpath = ".//*[@type='button']")
	WebElement calendarCloseButton;

	@FindBy(id = "search-button")
	WebElement searchButton;

	WebDriverWait wait;
	
	Actions action ;

	public HotelsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 10);
	    action= new Actions(driver);
	    
	}

	public void clickHotels() {
		hotelButton.click();
		hotelOnlyButton.click();
	}

	public void hotelDestinationActivity() throws Exception {
		hotelDestination.click();
		Thread.sleep(1000);
		hotelDestination.sendKeys("Pune");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("aria-option-0")));
		dropDownOptionDestination.click();
		hotelDestination.sendKeys(Keys.ARROW_DOWN);
		//hotelDestination.sendKeys(Keys.ENTER);
		System.out.println("Entered pune in the searchbox");

	}

	public void selectingHotelDates() {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 0);
		date = calendar.getTime();
		String stringCheckInDate = format.format(date);
		System.out.println(stringCheckInDate);
		//wait.until(ExpectedConditions.elementToBeClickable(hotelCheckInDate));
		
		//action.click(hotelCheckInDate).perform();
		hotelCheckInDate.click();
		hotelCheckInDate.sendKeys(stringCheckInDate);
		System.out.println("Selected checkin date");

		/*
		 * Picking the date assigning and passing into checkout date field
		 */

		hotelCheckOutDate.clear();
		hotelCheckOutDate.click();
		calendar.add(Calendar.DATE, 2);
		date = calendar.getTime();
		String stringCheckOutDate = format.format(date);
		System.out.println(stringCheckOutDate);
		hotelCheckOutDate.sendKeys(stringCheckOutDate);
		System.out.println("Selected checkout date");
		calendarCloseButton.click();/* Close button of displayed calendar */

	}

	/* Clicking on search button */
	public void clickSearchButton() {
		searchButton.click();
		System.out.println("Cliked on search");
	}

	public void hotelBooking() throws Exception {
		clickHotels();
		hotelDestinationActivity();
		selectingHotelDates();
		clickSearchButton();
	}
}
